package pl.pkobp.projects.dao;

import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;

import java.util.List;

public interface ProjectDao {

    List<Project> findAll();

    Project findById(int id);

    Project findByName(String name);

    void addProject(Project p);



}
