package pl.pkobp.projects.dao;

import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;
import pl.pkobp.projects.model.Task;

import java.util.List;

public interface TaskDao {

    List<Task> findAll();

    Task findById(int id);

    List<Task> findByStatus(Task.Status status);

    List<Task> findNotAssigned();

    List<Task> findByEmployee(Employee e);

    List<Task> findByProject(Project p);

    void addTask(Task task);
}
