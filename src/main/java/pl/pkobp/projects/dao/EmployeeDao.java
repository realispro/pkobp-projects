package pl.pkobp.projects.dao;

import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;

import java.util.List;

public interface EmployeeDao {

    List<Employee> findAll();

    Employee findById(int id);

    void addEmployee(Employee e);

    List<Employee> findByProject(Project p);

}
