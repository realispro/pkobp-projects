package pl.pkobp.projects.dao;

import pl.pkobp.projects.dao.jdbc.JdbcProjectDao;
import pl.pkobp.projects.model.Project;

public class DaoApp {

    public static void main(String[] args) {
        ProjectDao dao = new JdbcProjectDao();

        Project p = new Project(102, "Project from DaoApp");
        dao.addProject(p);
    }
}
