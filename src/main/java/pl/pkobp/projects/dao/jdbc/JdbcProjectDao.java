package pl.pkobp.projects.dao.jdbc;

import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.model.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcProjectDao implements ProjectDao {

    @Override
    public List<Project> findAll() {

        try(Connection c = ConnectionUtil.getConnection();){

            List<Project> projects = new ArrayList<>();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("SELECT ID,NAME FROM PROJECT");
            while (rs.next()){
                int id = rs.getInt("ID");
                String name = rs.getString("NAME");
                projects.add(new Project(id, name));
            }
            return projects;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public Project findById(int id) {
        return null;
    }

    @Override
    public Project findByName(String name) {
        return null;
    }

    @Override
    public void addProject(Project p) {

        try(Connection connection = ConnectionUtil.getConnection()){

            connection.setAutoCommit(false);

            PreparedStatement s  = connection.prepareStatement(
                    "INSERT INTO PROJECT(ID,NAME) VALUES (?,?)");
            s.setInt(1, p.getId());
            s.setString(2,p.getName());

            int count = s.executeUpdate();
            System.out.println("count = " + count);

            connection.commit();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
