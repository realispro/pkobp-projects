package pl.pkobp.projects.dao.inmemory;

import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.model.Project;

import java.util.List;

public class ProjectDaoImpl implements ProjectDao {


    @Override
    public List<Project> findAll() {
        return Data.projects;
    }

    @Override
    public Project findById(int id) {
        return Data.projects.stream()
                .filter(p->p.getId()==id)
                .findFirst().get();
    }

    @Override
    public Project findByName(String name) {
        return Data.projects.stream()
                .filter(p->p.getName()==name)
                .findFirst().orElse(null);
    }

    @Override
    public void addProject(Project p) {
        p.setId(nextId());
        Data.projects.add(p);
    }


    private int nextId(){
        return Data.projects.stream()
                .max((p1,p2)->p1.getId()-p2.getId())
                .get().getId()+1;
    }
}
