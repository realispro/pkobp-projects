package pl.pkobp.projects.dao.inmemory;

import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;
import pl.pkobp.projects.model.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Data {

    static List<Project> projects = new ArrayList<>();

    static List<Task> tasks = new ArrayList<>();

    static List<Employee> employees = new ArrayList<>();

    static {

        Employee kowalski = new Employee(1, "Jan", "Kowalski");
        Employee nowak = new Employee(2, "Adam", "Nowak");
        Employee doe = new Employee(3, "Joe", "Doe");
        Employee smith = new Employee(4, "John", "Smith");

        Project internalTool = new Project(1, "Internal Tool");
        Project critical = new Project(2, "Critical Project");

        Task gui = new Task(1, critical, "Graphical User Interface", LocalDateTime.now());
        Task service = new Task(2, critical, "Business Service", LocalDateTime.now());
        Task dao = new Task(3, critical, "Data Access Object", LocalDateTime.now());
        Task model = new Task(4, critical, "Data Model", LocalDateTime.now());

        Task task1 = new Task(5, internalTool, "Task1", LocalDateTime.now());
        Task task2 = new Task(6, internalTool, "Task2", LocalDateTime.now());
        Task task3 = new Task(7, internalTool, "Task3", LocalDateTime.now());

        //model.setStatus(Task.Status.CLOSED);
        dao.setStatus(Task.Status.PENDING);
        task3.setStatus(Task.Status.CLOSED);
        task2.setStatus(Task.Status.PENDING);

        model.setEmployee(kowalski);
        dao.setEmployee(nowak);
        service.setEmployee(doe);

        task2.setEmployee(nowak);
        task3.setEmployee(doe);

        kowalski.getProjects().add(critical);
        nowak.getProjects().add(critical);
        doe.getProjects().add(critical);
        nowak.getProjects().add(internalTool);
        doe.getProjects().add(internalTool);

        employees.add(kowalski);
        employees.add(nowak);
        employees.add(doe);
        employees.add(smith);

        projects.add(internalTool);
        projects.add(critical);

        tasks.add(gui);
        tasks.add(service);
        tasks.add(dao);
        tasks.add(model);
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

    }
}
