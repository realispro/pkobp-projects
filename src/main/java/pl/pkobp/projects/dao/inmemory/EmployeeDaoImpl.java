package pl.pkobp.projects.dao.inmemory;

import pl.pkobp.projects.dao.EmployeeDao;
import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDaoImpl implements EmployeeDao {
    @Override
    public List<Employee> findAll() {
        return Data.employees;
    }

    @Override
    public Employee findById(int id) {
        return Data.employees.stream()
                .filter(e -> e.getId() == id)
                .findFirst().get();
    }

    @Override
    public void addEmployee(Employee e) {
        e.setId(nextIde());
        Data.employees.add(e);
    }

    private int nextIde() {
        return Data.employees.stream()
                .max((e1, e2) -> e1.getId() - e2.getId())
                .get().getId() + 1;
    }

    @Override
    public List<Employee> findByProject(Project p) {
        /*List<Employee> employeesInProject = new ArrayList<>();
        for (Employee e : Data.employees) {
            if (e.getProjects().contains(p)) {
                employeesInProject.add(e);
            }
        }
        return employeesInProject;*/

        return Data.employees.stream()
                .filter(e->e.getProjects().contains(p))
                .collect(Collectors.toList());
    }
}
