package pl.pkobp.projects.dao.inmemory;

import pl.pkobp.projects.dao.TaskDao;
import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;
import pl.pkobp.projects.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskDaoImpl implements TaskDao {

    @Override
    public List<Task> findAll() {
        return Data.tasks;
    }

    @Override
    public Task findById(int id) {
        return Data.tasks.stream()
                .filter(t -> t.getId() == id)
                .findFirst().get();
    }

    @Override
    public List<Task> findByStatus(Task.Status status) {
        return Data.tasks.stream()
                .filter(t->t.getStatus()==status)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findNotAssigned() {
        return Data.tasks.stream()
                .filter(t->t.getEmployee()==null)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findByEmployee(Employee e) {
        return Data.tasks.stream()
                .filter(t->e.equals(t.getEmployee()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> findByProject(Project p) {
        return Data.tasks.stream()
                .filter(t->p.equals(t.getProject()))
                .collect(Collectors.toList());
    }

    @Override
    public void addTask(Task task) {
        task.setId(nextId());
        Data.tasks.add(task);
    }

    private int nextId() {
        return Data.tasks.stream()
                .max((e1, e2) -> e1.getId() - e2.getId())
                .get().getId() + 1;
    }
}
