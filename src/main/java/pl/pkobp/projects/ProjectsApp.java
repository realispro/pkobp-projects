package pl.pkobp.projects;

import pl.pkobp.projects.service.ProjectException;
import pl.pkobp.projects.service.ProjectService;
import pl.pkobp.projects.service.ReportingService;
import pl.pkobp.projects.service.impl.ProjectServiceImpl;

public class ProjectsApp {


    public static void main(String[] args) throws ProjectException {
        System.out.println("ProjectsApp.main");

        ProjectService projectService = new ProjectServiceImpl();
        ReportingService reportingService = null;

        int projectId = projectService.createNewProject("Fancy startup project");
        int employeeId = projectService.employPerson("Sylvester", "Stallone");
        projectService.assignToProject(employeeId,projectId);

        int taskId = projectService.createTask(projectId, "Setup environment");
        projectService.assignTask(taskId, employeeId);

        projectService.startWork(taskId);
        projectService.finishTask(taskId);

        System.out.println("done.");


    }
}
