package pl.pkobp.projects.service;

public interface ProjectService {

    /**
     * creates new project
     * @param name
     * @return
     * @throws ProjectException - if name not unique
     */
    int createNewProject(String name) throws ProjectException;

    /**
     *
     * @param firstName
     * @param lastName
     * @return
     * @throws ProjectException
     */
    int employPerson(String firstName, String lastName) throws ProjectException;


    int createTask(int projectId, String topic) throws ProjectException;

    /**
     * Assigns employee to project
     * @param employeeId
     * @throws ProjectException
     */
    void assignToProject(int employeeId, int projectId) throws ProjectException;

    /**
     * Assignes employee to task
     * @param taskId
     * @param employeeId
     * @throws ProjectException - if employee not assigned to task's project
     */
    void assignTask(int taskId, int employeeId) throws ProjectException;

    /**
     * starts work on task - status changes to PENDING
     * @param taskId
     * @throws ProjectException - if task already closed
     */
    void startWork(int taskId) throws ProjectException;


    /**
     * finishinh task - status change + timestamp
     * @param taskId
     * @throws ProjectException - if not in PENDING state
     */
    void finishTask(int taskId) throws ProjectException;

}
