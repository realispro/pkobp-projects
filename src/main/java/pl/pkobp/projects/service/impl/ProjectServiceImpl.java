package pl.pkobp.projects.service.impl;

import pl.pkobp.projects.dao.EmployeeDao;
import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.dao.TaskDao;
import pl.pkobp.projects.dao.inmemory.EmployeeDaoImpl;
import pl.pkobp.projects.dao.inmemory.ProjectDaoImpl;
import pl.pkobp.projects.dao.inmemory.TaskDaoImpl;
import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;
import pl.pkobp.projects.model.Task;
import pl.pkobp.projects.service.ProjectException;
import pl.pkobp.projects.service.ProjectService;

import java.time.LocalDateTime;

public class ProjectServiceImpl implements ProjectService {

    private ProjectDao projectDao = new ProjectDaoImpl();

    private EmployeeDao employeeDao = new EmployeeDaoImpl();

    private TaskDao taskDao = new TaskDaoImpl();

    @Override
    public int createNewProject(String name) throws ProjectException {
        if(projectDao.findByName(name)!=null){
            throw new ProjectException("project name already used");
        }
        Project p = new Project(-1, name);
        projectDao.addProject(p);
        return p.getId();
    }

    @Override
    public int employPerson(String firstName, String lastName) throws ProjectException {
        Employee e = new Employee(-1, firstName, lastName);
        employeeDao.addEmployee(e);
        return e.getId();
    }

    @Override
    public int createTask(int projectId, String topic) throws ProjectException {
        Task task = new Task(-1, projectDao.findById(projectId), topic, LocalDateTime.now());
        taskDao.addTask(task);
        return task.getId();
    }

    @Override
    public void assignToProject(int employeeId, int projectId) throws ProjectException {
        Employee e = employeeDao.findById(employeeId);
        Project p = projectDao.findById(projectId);
        e.getProjects().add(p);
    }

    @Override
    public void assignTask(int taskId, int employeeId) throws ProjectException {
        Task task = taskDao.findById(taskId);
        Employee employee = employeeDao.findById(employeeId);
        task.setEmployee(employee);
    }

    @Override
    public void startWork(int taskId) throws ProjectException {
        Task task = taskDao.findById(taskId);
        if(task.getStatus()!= Task.Status.OPEN){
            throw new ProjectException("can not start task which is not in open state");
        }
        task.setStatus(Task.Status.PENDING);
    }

    @Override
    public void finishTask(int taskId) throws ProjectException {
        Task task = taskDao.findById(taskId);
        if(task.getStatus()!= Task.Status.PENDING){
            throw new ProjectException("can not start task which is not in pending state");
        }
        task.setStatus(Task.Status.CLOSED);
        task.setClosed(LocalDateTime.now());
    }
}
