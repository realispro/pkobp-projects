package pl.pkobp.projects.service;

import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Task;

import java.util.List;

public interface ReportingService {

    List<Employee> employeesWithoutAssignement();

    Task theOldestTask();

    

}
