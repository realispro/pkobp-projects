package pl.pkobp.projects.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Task {

    private int id;

    private Project project;

    private String topic;

    private Status status = Status.OPEN;

    private LocalDateTime created;

    private LocalDateTime closed;

    private Employee employee;

    public Task(int id, Project project, String topic, LocalDateTime created) {
        this.id = id;
        this.project = project;
        this.topic = topic;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getClosed() {
        return closed;
    }

    public void setClosed(LocalDateTime closed) {
        this.closed = closed;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(topic, task.topic) &&
                status == task.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, topic, status);
    }

    public enum Status {
        OPEN,
        PENDING,
        CLOSED
    }

}
