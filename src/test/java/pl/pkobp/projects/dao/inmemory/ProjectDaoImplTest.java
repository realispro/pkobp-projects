package pl.pkobp.projects.dao.inmemory;

import org.junit.Test;
import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.model.Project;

import static org.junit.Assert.*;

public class ProjectDaoImplTest {

    ProjectDao dao = new ProjectDaoImpl();

    @Test
    public void testFindAll() {
        assertEquals(2, dao.findAll().size());
    }

    @Test
    public void testFindById() {
        assertEquals(new Project(1, "Internal Tool"), dao.findById(1));
    }


    @Test
    public void testAddProject() {
        Project p = new Project(-1, "Fake Project");
        dao.addProject(p);
        assertEquals(3, p.getId());
    }

    @Test
    public void testFindByName() {
        assertEquals(1,dao.findByName("Internal Tool").getId());
    }
}
