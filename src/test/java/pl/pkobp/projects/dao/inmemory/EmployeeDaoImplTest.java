package pl.pkobp.projects.dao.inmemory;

import org.junit.Test;
import pl.pkobp.projects.dao.EmployeeDao;
import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Project;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class EmployeeDaoImplTest {

    EmployeeDao dao = new EmployeeDaoImpl();

    @Test
    public void testFindAll() {
        assertEquals(4, dao.findAll().size());
    }

    @Test
    public void testFindById() {
        assertEquals(
                new Employee(2, "Adam", "Nowak"),
                dao.findById(2)
        );
    }


    @Test
    public void testAddEmployee() {
        Employee e = new Employee(-1, "Gal", "Anonim");
        dao.addEmployee(e);
        assertEquals(5, e.getId());
    }


    @Test
    public void testFindByProject() {
        assertEquals(
                Arrays.asList(
                        new Employee(1, "Jan", "Kowalski"),
                        new Employee(2, "Adam", "Nowak"),
                        new Employee(3, "Joe", "Doe")
                ),
                dao.findByProject(new Project(2, "Critical Project"))
        );
    }




}
