package pl.pkobp.projects.dao.inmemory;

import org.junit.Test;
import pl.pkobp.projects.dao.TaskDao;
import pl.pkobp.projects.model.Employee;
import pl.pkobp.projects.model.Task;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TaskDaoImplTest {

    TaskDao dao = new TaskDaoImpl();

    @Test
    public void testFindAll() {
        assertEquals(7, dao.findAll().size());
    }

    @Test
    public void testFindById() {
        assertEquals(
                new Task(1, null, "Graphical User Interface", LocalDateTime.now()),
                dao.findById(1)
        );
    }

    @Test
    public void testFindByEmployee() {
        assertEquals(
                Arrays.asList(new Task(4, null, "Data Model", LocalDateTime.now())),
                dao.findByEmployee(new Employee(1, "Jan", "Kowalski"))
        );
    }


    @Test
    public void testZAddTask() {
        Task e = new Task(-1, null, "Graphical User Interface", LocalDateTime.now());
        dao.addTask(e);
        assertEquals(8, e.getId());
    }


}
