package pl.pkobp.projects.dao.jdbc;

import org.junit.Test;
import pl.pkobp.projects.dao.ProjectDao;
import pl.pkobp.projects.model.Project;

import java.util.List;

import static org.junit.Assert.*;

public class JdbcProjectDaoTest {

    private ProjectDao projectDao = new JdbcProjectDao();

    @Test
    public void findAll() {

        List<Project> projects = projectDao.findAll();

        System.out.println("projects: " + projects);

    }

    @Test
    public void addProject(){
        Project p = new Project(101, "Project from test class");
        projectDao.addProject(p);
    }
}
